# Machine Learning 1
Assignments for Machine Learning I: Foundations taught by Prof. Marius Kloft and TA Rodrigo Alves at Technische Universität Kaiserslautern taken in Summer semester 2019.

### Folder structure
Each exercise will contain the following structure:  
```
├── ML_EX[n]        # Top directory for each exercise
│   ├ data_sets     #   Contains all required datasets for exercise
│   ├ docs          #   Exercise, team solutions, and final solutions
│   ├ figures       #   Snapshots of required figures for reports
│   ├ scripts       #   Code for exercise
│   ├ README.md     #   Optional README
├── Resources       # Contains libraries commonly used
└── README.md
```

**Note: Read the documents in ML_EX[n]/docs for assignment instructions.**

### Group Members
```
Mauricio Solis [solisjr@rhrk.uni-kl.de]
Shrutika Jain  [jain@rhrk.uni-kl.de]
Tayyaba Seher  [seher@rhrk.uni-kl.de]
```
