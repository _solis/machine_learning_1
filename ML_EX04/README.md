# Exercise 4

### Folder structure
Each exercise will contain the following structure:  
```
├── data_sets     #   Contains all required datasets for exercise
├── docs          #   Exercise sheet and team solutions PDF
├── figures       #   Snapshots of required figures for reports
├── scripts       #   Code for exercise
└── README.md
```

### Group Members
```
Mauricio Solis [solisjr@rhrk.uni-kl.de]
Shrutika Jain  [jain@rhrk.uni-kl.de]
Tayyaba Seher  [seher@rhrk.uni-kl.de]
```

### Usage
In the scripts/ directory. Run the "ex4_run.sh" script.

It is assumed that it will be running on a bash shell, that Python 2 and 3 are installed on the running computer, and that LIBLINEAR is also installed.

### Notes
**It is assumed "BIG_DWH_Training.csv" is added by user in data_sets/ (file too large to upload)**

phraug2-master was used to convert both from CSV to LIBSVM and vowpal wabbit formats.  Modifactions had to be done to both scripts as they would not run "out of the box" for given assignment files. 

For LIBSVM, format is as follows [label] [index]:[value1] [index2]:[value2] ...
