#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import math
from random import seed
from random import randint
from time import sleep

# Read CSV files
training_data_df = pd.read_csv('../data_sets/BIG_DWH_Training.csv', delimiter=',', 
                        names=['index', 'height', 'weight', 'gender'])
del training_data_df['index']
training_data_np = training_data_df.values
# Indexes
HEIGHT = 0
WEIGHT = 1
GENDER = 2
MALE   = 0
FEMALE = 1
ITERATIONS = 10
############################################################################################################
# 3a)
#   Calculate the gradient vector for the SVM (relative to w and b)
#   Gradient vectors (partial derivaties in respect to w and b:
#       if (1 - y_i(wT*x_i + b) >= 0)
#           df/dw = w - C * sum(y_i*x_i)
#           df/db = -C * sum(y_i)
#       else
#           0
############################################################################################################
############################################################################################################
# 3b)
#   Use the output of NCC to initialize your w0 and b0 .
############################################################################################################
def compute_centroid(data):
    # Get male and female x/y values
    male      = data.loc[data['gender'] == 1]
    female    = data.loc[data['gender'] == -1]
    # Compute averges
    male    = np.mean(male)
    female  = np.mean(female)
    # No longer needed
    del male['gender']
    del female['gender']
    # Return centroids as tuple
    # male/female[0] = average for height
    # male/female[1] = average for weight
    return [male,female]
    
def calc_linear_classifier(center_m, center_f, x_data):
    x = 0
    y = 1
    A = 2*(center_f[x] - center_m[x])
    B = 2*(center_f[y] - center_m[y])
    C = center_m[x]**2 + center_m[y]**2 - center_f[x]**2 - center_f[y]**2
    slope = -(A/B)
    inter = -(C/B)
    label = "{:.2f}*x + {:.2f}".format(slope,inter)
    return [(slope*x_data + inter),label, A, B, C]
    
centroid = compute_centroid(training_data_df)
classifier,label,iw1,iw2,ib =                   \
    calc_linear_classifier(centroid[MALE],      \
                           centroid[FEMALE],    \
                           training_data_df['height'].values)

print("NCC:")
print("  Hyperplane: {:.2f}x + {:.2f}y + {:.2f}".format(iw1, iw2, ib))
print("  Classifier: {}".format(label))
'''
NCC:
  Hyperplane: 11.08x + 16.61y + -2976.81
  Classifier: -0.67*x + 179.20
'''
# CV
#  Params:
#   t = CV iterations
#   kfolds = number of folds
#   Data = as numpy dataframe
def cross_validation(t, kfolds, data):
    # Folding size = num_rows / k (folds)
    fold_size       = int(len(data) / kfolds)
    dataset_split   = list()
    for i in range(t):
        seed()
        sleep(0.01)
        dataset_copy    = data.values.tolist()
        test_set        = list()
        train_set       = list()
        for j in range(kfolds):
            fold = list()
            # Randomly split the samples into k sets of the same size (“folds”)
            while len(fold) < fold_size:
                index = randint(0, len(dataset_copy)-1)
                fold.append(dataset_copy.pop(index))
            dataset_split.append(fold)
            # Use jth fold as test set and union of all remaining folds as training set
            if i == j:
                test_set.append(dataset_split[j])
            else:
                train_set.append(dataset_split[j])
                
        # Train classifier on training set and predict on test set
        train_split = np.array(train_set)
        test_split  = np.array(test_set)
        return [train_split,test_split]
# SGD
#  Params:
#   T = SGD iterations
#   C = Regularisation parameter
#   B = Batch size
#   i[w1,w2,b] = Initial hyperplane parameters
#   Data = as numpy array
def stochastic_gradient_descent(T, C, B, iw1, iw2, ib, data):
    # Initialize
    w1 = iw1
    w2 = iw2
    b  = ib
    for t in range(1,T+1):
        w1_sums = 0
        w2_sums = 0
        b_sums  = 0
        # Seed for random
        seed()
        sleep(0.01)
        # Holds the randomly selected batches, [height,weight,gender]
        batch   = np.empty((B,3))
        # Holds terms for later summation, [(y_i * x1_i), (y_i * x2_i), y_i]
        terms   = np.empty((B,3))

        # Randomly select b many data points
        for i in range(len(data)):
            for b in range(B):
                # Random selection
                index = randint(0, len(data[i])-1)
                batch[b] = data[i][index]
                # Check if, 1 - yi(wT*xi + b) >= or < 0
                check = \
                    (batch[b][GENDER]*((iw1*batch[b][HEIGHT]) + \
                    (iw2*batch[b][WEIGHT]) + ib))
                if check < 1:
                    # Get all the terms for partial derivatives, [w1/w2(0/1), b(2)]
                    terms[b][0] = batch[b][HEIGHT] * batch[b][GENDER]
                    terms[b][1] = batch[b][WEIGHT] * batch[b][GENDER]
                    terms[b][2] = batch[b][GENDER]
                else:
                    terms[b][0] = 0
                    terms[b][1] = 0
                    terms[b][2] = 0
                
                # Sum
                w1_sums += terms[b][0]
                w2_sums += terms[b][1]
                b_sums  += terms[b][2]
                
            # Multiply C accordiingly
            gw1 = iw1 - (C * w1_sums)
            gw2 = iw2 - (C * w2_sums)
            gb  = -C * b_sums
            
            # Set learning rate
            lamda = 1 / t
            w1 = w1 - (lamda*gw1)
            w2 = w2 - (lamda*gw2)
            b  = b - (lamda*gb)

            # Update x
            x_t = (w1*data[i][index][HEIGHT]) + (w2*data[i][index][WEIGHT]) + b
        
    # Return final x_t and w1, w2, b
    return [x_t,w1,w2,b]
#############################################################################################################
# 3b)
#  Consider the learning rate λ_t = 1/t, the number iterations of SGD T = 10000 and using the
#  training set “DWH Training.csv” find the best value of C ∈ {0.1, 1, 10} and B ∈ {1, 10, 50}
#  (the size of the batch) via 10-fold-crossvalidation.
#############################################################################################################   
# Converts to mx + b form given A*x, B*y, and C
def convert_to_mx_b(A, B, C):
    slope = -(A/B)
    inter = -(C/B)
    return [slope,inter]
# Determines the accuracy based on the ground truth data
def calc_accuracy(data, classified):
    correct_classified  = 0
    error_classified    = 0
    for i in range(len(classified)):
        for k in range(len(classified[i])):
            # Correct classification first
            if data[i][k][GENDER] == 1 and classified[i][k] == 'male':
                correct_classified += 1
            elif data[i][k][GENDER] == -1 and classified[i][k] == 'female':
                correct_classified += 1
            # Erroneous classification
            # Should be female but classified as male
            elif data[i][k][GENDER] == -1 and classified[i][k] == 'male':
                error_classified += 1
            # Should be male but classified as femal
            elif data[i][k][GENDER] == 1 and classified[i][k] == 'female':
                error_classified += 1
            # We should not go here
            else:
                error_classified += 1
    # Calculate accuracy
    acc = (correct_classified / len(classified[0]))*100
    return [acc,correct_classified,error_classified]
# Calculates distance to hyperplane
#    d[ ] = (m*(x) - y + cn) / sqrt(m^2 + 1)
# If the signed distance is negative then classify 
# the citizen as Male. Else, classify as Female.
def classify(data, slope, intersection):
    distances = list()
    # Calculate distance
    for i in range(len(data)):
        distances.append([])
        for k in range(len(data[i])):
            x = data[i][k][HEIGHT]
            y = data[i][k][WEIGHT]
            slope_calc      = math.sqrt(slope**2 + 1)
            slope_times_x   = slope*x
            dist = ((slope_times_x - y + intersection) / slope_calc)
            # Calculate distances
            distances[i].append(dist)

    classified = list()
    # For all calculated distances
    for i in range(len(distances)):
        classified.append([])
        for k in range(len(distances[i])):
            if distances[i][k] >= 0:
                classified[i].append("male")
            else:
                classified[i].append("female")
    return classified
# Driver code
C = [0.1,1,10]
B = [1,10,50]
temp = []
for c in range(3):
    for i in range(3):
        # Split data, returns [train_split,test_split]
        train_split,test_split = cross_validation(1, 10, training_data_df)
        # Get the hyperplane and its parameters
        hp,w1,w2,b = stochastic_gradient_descent(ITERATIONS, C[c], B[i], iw1, iw2, ib, train_split)
        # Convert to mx+b
        slope,inter = convert_to_mx_b(w1,w2,b)
        classification = classify(test_split, slope, inter)
        accuracy,correct,error = calc_accuracy(test_split, classification)
        # Print
        #print("  C = {}, B = {}:".format(C[c],B[i]))
        #print("    {:.2f}*x + {:.2f}*y + {:.2f}".format(w1,w2,b))
        #print("    mx+b form:")
        #print("     {:.2f}*x + {:.5f}".format(slope,inter))
        #print("    Classification:")
        #print("     Correct:   {}".format(correct))
        #print("     Incorrect: {}".format(error))
        #print("     Accuracy: {:.2f}%".format(accuracy))
        temp.append([C[c],B[i],w1,w2,b,slope,inter,correct,error,accuracy])
# Find the best hyperplane so we can plot
results = np.array(temp)
best = 0
loc = 0
for i in range(len(results)):
    if best < results[i][9]:
        best = results[i][9]
        loc = i
print("Best Hyperplane:")
print("  C = {}, B = {}:".format(results[loc][0],results[loc][1]))
print("    {:.2f}*x + {:.2f}*y + {:.2f}".format(results[loc][2],results[loc][3],results[loc][4]))
print("    mx+b form:")
print("     {:.2f}*x + {:.5f}".format(results[loc][5],results[loc][6]))
print("    Classification:")
print("     Correct:   {}".format(results[loc][7]))
print("     Incorrect: {}".format(results[loc][8]))
print("     Accuracy: {:.2f}%".format(results[loc][9]))
#############################################################################################################
# 3c)
#  Make a scatter plot where the x-axis is the height of the citizens and the y-axis is the weight
#  of the citizens. The color of the points need to be different for males and females. In the same
#  figure, plot the linear classifier calculated.
#############################################################################################################  
# Get male data
males = training_data_df.loc[training_data_df['gender'] == 1]
male_x_data = males['height']
male_y_data = males['weight']
# Get female data
females = training_data_df.loc[training_data_df['gender'] == -1]
female_x_data = females['height']
female_y_data = females['weight']
# Plot the scatter
plt.scatter(male_x_data, male_y_data, color='b', label='male')
plt.scatter(female_x_data, female_y_data, color='r', label='female')
plt.xlabel('Height (cm)')
plt.ylabel('Weight (kg)')
plt.title('Disnyland Population Weight and Height by Gender')
# Make the hyperplane
x = training_data_df['height'].values
# Scale linear classifier up to match the
# already plotted male/female points
f = results[loc][5]*x + results[loc][6]
MaxVal  = np.amax(training_data_df['weight'].values)
MinVal  = np.amin(training_data_df['weight'].values)
f = np.interp(f, (f.min(), f.max()), (MinVal, MaxVal))
# Plot

label = "{:.2f}*x + {:.5f}".format(results[loc][5],results[loc][6])
plt.plot(x, f, color='k', label=label)
plt.legend(loc='lower left')
plt.show()
'''
3c/d
Best Hyperplane:
  C = 0.1, B = 10.0:
    37416.47*x + 12564.20*y + 9.00
    mx+b form:
     -2.98*x + -0.00072
    Classification:
     Correct:   15.0
     Incorrect: 8.0
     Accuracy: 65.22%

'''
'''
3e
NCC:
  Hyperplane: 11.34x + 18.92y + -3175.86
  Classifier: -0.60*x + 167.86
Best Hyperplane:
  C = 0.1, B = 1.0:
    793.64*x + -98.84*y + 0.04
    mx+b form:
     8.03*x + 0.00040
    Classification:
     Correct:   102900.0
     Incorrect: 97100.0
     Accuracy: 51.45%
'''
