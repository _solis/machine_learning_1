#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
from scipy import sparse
import pandas as pd
import sys
sys.path.insert(0, '../../Resources/liblinear-master/python/')
from liblinearutil import *

# Read CSV file
training_data \
    = pd.read_csv('../data_sets/BIG_DWH_Training.csv',  \
        delimiter=',',                                  \
        names=['index', 'height', 'weight', 'gender'])
                    
# Get male data
males = training_data.loc[training_data['gender'] == 1]
male_x_data = males['height'].values
male_y_data = males['weight'].values
# Get female data
females = training_data.loc[training_data['gender'] == -1]
female_x_data = females['height'].values
female_y_data = females['weight'].values

# Read data in LIBSVM format, here _y refers lables
# _x refers to data [height/weight]
train_y, train_x = \
    svm_read_problem('../data_sets/BIG_DWH_Training.svm', \
    return_scipy=True)
test_y, test_x = \
    svm_read_problem('../data_sets/DWH_test.svm', \
    return_scipy=True)

# Scale training and test data, these values will
# be used further below
scale_param     = csr_find_scale_param(train_x, lower=0, upper=1)
scaled_train_x  = csr_scale(train_x, scale_param)
scale_param     = csr_find_scale_param(train_x, lower=0, upper=1)
scaled_test_x   = csr_scale(test_x, scale_param)

############################################################################################################
# a)
#   Make a scatter plot where the x-axis is the height of the citizens and the y-axis is the weight
#   of the citizens. The color of the points need to be different for males and females. In the same
#   figure, plot the linear classifier calculated with LIBLINEAR
############################################################################################################
plt.scatter(male_x_data, male_y_data, color='b', label='male')
plt.scatter(female_x_data, female_y_data, color='r', label='female')
plt.xlabel('Height (cm)')
plt.ylabel('Weight (kg)')
plt.title('Disnyland Population Weight and Height by Gender')

# Construct a problem instance
prob_train          = problem(train_y, scaled_train_x)
# Training
param               = parameter('-s 0 -B 100 -q')
model               = train(prob_train, param)
# Prediction
p_label,p_acc,p_val = predict(test_y, scaled_test_x, model)
ACC, MSE, SCC       = evaluations(test_y, p_label)
# Save model
save_model('../data_sets/DWH.model', model)

# Create the hyperplane, Ax + By + C = 0
x_linespace = training_data['height']
w1 = model.get_decfun_coef(feat_idx=1)
w2 = model.get_decfun_coef(feat_idx=2)
b  = model.get_decfun_bias()

# Convert to mx + b
slope   = -(w1/w2)
inter   = -(b/w2)

# Output some stuff
print("Linear Classifier:")
label  = "{:.2f}*x + {:.2f}y + {:.2f}".format(w1,w2,b)
print("   {}".format(label))
print(" as mx + b form:")
label  = "{:.2f}*x + {:.2f}".format(slope,inter)
print("   {}".format(label))

# Scale linear classifier up to match the
# already plotted male/female points
f       = slope*x_linespace + inter
MaxVal  = np.amax(training_data['weight'].values)
MinVal  = np.amin(training_data['weight'].values)
f = np.interp(f, (f.min(), f.max()), (MinVal, MaxVal))
# Plot
plt.plot(x_linespace, f, color='k', label=label)

plt.legend(loc='lower left')
plt.show()
'''
Accuracy = 86.6667% (39/45) (classification)
Linear Classifier:
   -22.83*x + -11.25y + 17.71
 as mx + b form:
   -2.03*x + 1.57
'''
