echo "Running Task 3b and 3c..."
python3 ex4_task3bc.py
echo "... done"

echo "Running Task 3e..."
echo "Converting DWH training and test data to LIBSVM format..."
python ../../Resources/phraug2-master/csv2libsvm.py ../data_sets/BIG_DWH_Training.csv ../data_sets/BIG_DWH_Training.svm -l3 -c0
python ../../Resources/phraug2-master/csv2libsvm.py ../data_sets/DWH_test.csv ../data_sets/DWH_test.svm -l3 -c0 -c3
python3 ex4_task3e.py
echo "...done"

