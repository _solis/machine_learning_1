#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import math
from random import randrange

# Read CSV file
training_data = pd.read_csv('../data_sets/DWH_Training.csv', delimiter=',', 
                    names=['index', 'height', 'weight', 'gender'])

# Get male data
males = training_data.loc[training_data['gender'] == 1]
male_x_data = males['height']
male_y_data = males['weight']
# Get female data
females = training_data.loc[training_data['gender'] == -1]
female_x_data = females['height']
female_y_data = females['weight']
# Plot the scatter
plt.scatter(male_x_data, male_y_data, color='b', label='male')
plt.scatter(female_x_data, female_y_data, color='r', label='female')
plt.xlabel('Height (cm)')
plt.ylabel('Weight (kg)')
plt.title('Disnyland Population Weight and Height by Gender')
############################################################################################################
# a)
#   Repeat the Exercise 2 (a). Add to this scatter plot all 5 possible hyperplanes that combine the
#   possible values of w with the possible values of b [{(0.576, 0.047), −103}, 
#   {(0.576, 0.047), −102}, · · · , {(0.576, 0.047), -99}
############################################################################################################
# Create 100 evenly spaced intervals, from 170 to 175
x = np.linspace(170, 175, 10)
# Calculated function m*x + cn
intersections = [2191.49, 2170.21, 2148.94, 2127.66, 2106.38]
slope = -12.25
lines = list()
colors = ['#FFC300', '#9933CC', '#39C700', '#C70039', '#CCFF33']
total_hyperplanes = len(intersections)
for i in range(total_hyperplanes):
    lines.append(slope*x + intersections[i])
    plt.plot(x, lines[i], color=colors[i], label=str(slope) + '*x + ' + str(intersections[i]))
    
plt.legend(loc='lower left')
plt.show()
