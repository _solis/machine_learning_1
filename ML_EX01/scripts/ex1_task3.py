#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import math
from random import randrange

# Read CSV file
training_data = pd.read_csv('../data_sets/DWH_Training.csv', delimiter=',', 
                    names=['index', 'height', 'weight', 'gender'])

# Get male data
males = training_data.loc[training_data['gender'] == 1]
male_x_data = males['height']
male_y_data = males['weight']
# Get female data
females = training_data.loc[training_data['gender'] == -1]
female_x_data = females['height']
female_y_data = females['weight']
############################################################################################################
# a)
#   Make a scatter plot where the x-axis is the height of the citizens and the y-axis is the weight of
#   the citizens. The color of the points need to be different for males and females.
############################################################################################################
plt.scatter(male_x_data, male_y_data, color='b', label='male')
plt.scatter(female_x_data, female_y_data, color='r', label='female')
plt.xlabel('Height (cm)')
plt.ylabel('Weight (kg)')
plt.title('Disnyland Population Weight and Height by Gender')
############################################################################################################
# b)
#   Draw a horizontal line for which you think that it best separates male and female citizens (you
#   don’t need to calculate it).
############################################################################################################
plt.axhline(y=65.5, color='black', linestyle=':', label='65.5kg h-guess')
############################################################################################################
# d)
#   Draw a vertical line for which you think that it best separates male and female citizens (again,
#   you don’t need to calculate it).
############################################################################################################
plt.axvline(x=171.5, color='black', linestyle='--', label='171.5cm v-guess')
############################################################################################################
# f)
#   Draw the line (once again, you don’t need to calculate it) for which you think that it best separates
#   male and female citizens.
############################################################################################################
guessed_x = np.linspace(165, 180, 10)
gusssed_m = -10
guessed_c = 1782
plt.plot(guessed_x, gusssed_m*guessed_x + guessed_c, color='c', label=str(gusssed_m) + '*x + ' + str(guessed_c) + ' guess')
############################################################################################################
# g)
#   Donald Duck has a lot of cousins. One of them weights 52kg and is 1.79cm tall. How would you
#   classify his cousin? Would you classify differently if you use the (b) or (d) lines?
############################################################################################################
plt.scatter(179, 52, s=100, color='g', label="Donald's cousin")
plt.legend(loc='lower left')
plt.show()
