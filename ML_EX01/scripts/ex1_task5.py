#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import math
from random import seed
from random import randint
from time import sleep

# Read CSV files
training_data = pd.read_csv('../data_sets/DWH_Training.csv', delimiter=',', 
                        names=['index', 'height', 'weight', 'gender'])
del training_data['index']
test_data = pd.read_csv('../data_sets/DWH_test.csv', delimiter=',', 
                        names=['index', 'height', 'weight', 'gender', 'unknown'])
del test_data['index']
del test_data['unknown']
############################################################################################################
# a)
#   Implement the algorithm leaving k as parameter
############################################################################################################
def KNN(training_data, test_data, k=10):
    train_x        = training_data['height'].values
    train_y        = training_data['weight'].values
    train_g        = training_data['gender'].values
    test_x         = test_data['height'].values
    test_y         = test_data['weight'].values
    # Create an array of test.rows by train.rows, so we're calculated the distance
    # between every test point to the training set
    distances      = np.zeros((test_x.size, train_x.size))
    nearest        = np.zeros((test_x.size, k))
    predictions    = []
    # Get the distance between training data and test data
    for i in range(test_x.size):
        for j in range(train_x.size):
            calc_x  = (test_x[i] - train_x[j])**2
            calc_y  = (test_y[i] - train_y[j])**2
            distances[i][j]     = math.sqrt(calc_x + calc_y)
    # Indirect sort for shortest distance - indeces are used to 'grab'
    # the values (distance and gender) of those citizens that are k-nearest
    indeces = np.argsort(distances, axis=1)
    # Get the the k-nearest
    for i in range(test_x.size):
        for j in range(k):
            nearest[i][j] = distances[i][indeces[i][j]]
    # Get votes and predict
    for i in range(test_x.size):
        maleVotes   = 0
        femaleVotes = 0
        for j in range(k):
            if train_g[indeces[i][j]] == 1:
                maleVotes += 1
            else:
                femaleVotes += 1
        #print("Male votes:   {}".format(maleVotes))
        #print("Female votes: {}".format(femaleVotes))
        if maleVotes > femaleVotes:
            #print("Prediction for {} is {}".format(i, "male"))
            predictions.append("male")
        else:
            #print("Prediction for {} is {}".format(i, "female"))
            predictions.append("female")
    return predictions
    
def calc_accuracy(data, classified):
    total_classified    = len(classified)
    correct_classified  = 0
    error_classified    = 0
    data_gender         = data['gender']
    for j in range(total_classified):
        # Correct classification first
        if data_gender[j] == 1 and classified[j] == 'male':
            correct_classified += 1
        elif data_gender[j] == -1 and classified[j] == 'female':
            correct_classified += 1
        # Erroneous classification
        # Should be female but classified as male
        elif data_gender[j] == -1 and classified[j] == 'male':
            error_classified += 1
        # Should be male but classified as femal
        elif data_gender[j] == 1 and classified[j] == 'female':
            error_classified += 1
        # We should not go here
        else:
            error_classified += 1
    # Calculate accuracy
    acc = (correct_classified / total_classified)*100
    return acc
# Run the code
knear = 10
predictions = KNN(training_data, test_data, knear)
# Find accuracy
acc = calc_accuracy(test_data, predictions)
print("5a): KNN, over {}-nearest".format(knear))
print(" Average of Accuracy: {:.5f}%".format(acc))
'''
5a): KNN, over 10-nearest
 Average of Accuracy: 86.66667%
'''
############################################################################################################
# b)
#   Consider k ∈ {3, 5, 20}. Using the dataset “DWH Training.csv”, use 10-fold crossvalidation (with
#   t = 1) to find the best value of the parameter k for the algorithm implemented in (a). See the
#   slide 49. Attention: note that the “k” in the k-nearest neighbor algorithm is different from the
#   “k” in “t-times k-fold cross validation”. In the later case, observe that k = 10
############################################################################################################
def CV(t, kfolds, training_data, knear):
    # Folding size = num_rows / k (folds)
    fold_size       = int(len(training_data) / kfolds)
    dataset_split   = list()
    accuracies      = np.zeros(t)
    for i in range(t):
        seed()
        sleep(0.01)
        dataset_copy    = training_data.values.tolist()
        test_set        = list()
        train_set       = list()
        for j in range(kfolds):
            fold = list()
            # Randomly split the samples into k sets of the same size (“folds”)
            while len(fold) < fold_size:
                index = randint(0, len(dataset_copy)-1)
                fold.append(dataset_copy.pop(index))
            dataset_split.append(fold)
            # Use jth fold as test set and union of all remaining folds as training set
            if i == j:
                test_set.append(dataset_split[j])
            else:
                train_set.append(dataset_split[j])
        # Train classifier on training set and predict on test set
        train_split = np.array(train_set)
        test_split  = np.array(test_set)
        # Convert to dataframe, expected by following functions
        train_df    = pd.DataFrame(train_split.reshape(-1, 3), columns=('height','weight','gender'))
        test_df     = pd.DataFrame(test_split.reshape(-1, 3), columns=('height','weight','gender'))
        train_df.reset_index(drop=True)
        test_df.reset_index(drop=True)
        predictions = KNN(train_df, test_df, knear)
        # Find accuracy
        accuracies[i] = calc_accuracy(test_df, predictions)
    print("5b: CV, over {}-times and {}-folds".format(t,kfolds))
    print(" Average of Accuracy:            {:.5f}%".format(np.average(accuracies)))
    print(" Standard Deviation of Accuracy: {:.5f}".format(np.std(accuracies)))
ks = (3, 5, 20)
for k in ks:
    CV(10, 10, training_data, k)
'''
5b: CV, over 10-times and 10-folds
 Average of Accuracy:            89.56522%
 Standard Deviation of Accuracy: 6.79152
5b: CV, over 10-times and 10-folds
 Average of Accuracy:            90.00000%
 Standard Deviation of Accuracy: 5.51677
5b: CV, over 10-times and 10-folds
 Average of Accuracy:            87.39130%
 Standard Deviation of Accuracy: 7.39130
'''
