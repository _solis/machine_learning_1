#!/usr/bin/env python3
import matplotlib.pyplot as plt
import pandas as pd
import math

############################################################################################################
# b)
#   Using Exercise 1, calculate the distance between each possible hyperplane and each citizen of the
#   training data.
#   params:
#       data: data as pandas dataframe
#       slope: calculated slope to hyperplane
#       intersections: intersections for mx+b formula, this equates to the number of hyperplanes
############################################################################################################
'''
Function
    d[ ] = (m*(x) - y + cn) / sqrt(m^2 + 1)
'''
def calc_dist_to_hp(data, slope, intersections):
    # Get data needed and pre-calculate some values
    x               = data['height']
    y               = data['weight']
    slope_calc      = math.sqrt(slope**2 + 1)
    slope_times_x   = slope*x
    distances       = []
    # Calculate distances
    for i in range(len(intersections)):
        distances.append(((slope_times_x - y + intersections[i]) / slope_calc))

    return distances

'''
ANSWER to 4b
'''
# Read CSV file
data = pd.read_csv('../data_sets/DWH_Training.csv', delimiter=',', 
                    names=['index', 'height', 'weight', 'gender'])
del data['index']
# Calculated from hyperplanes
intersections = [2191.49, 2170.21, 2148.94, 2127.66, 2106.38]
slope = -12.25
calc_dist_to_hp(data, slope, intersections)
############################################################################################################
# c)
#   If the signed distance is negative then classify the citizen as Male. Else, classify as Female.
#   params:
#       calc_distances: Calculated distances to hyperplane
#       num_of_hp: Number of hyperplanes
############################################################################################################
def classify_distance(calc_distances, num_of_hp):
    classified = []
    # For all hyperplanes
    for i in range(num_of_hp):
        classified.append([])
        # For all rows (points)
        for j in range(len(calc_distances[i].index)):
            if calc_distances[i].iloc[j] >= 0:
                classified[i].append("male")
            else:
                classified[i].append("female")
    return classified

'''
ANSWER to 4c
'''
distance = calc_dist_to_hp(data, slope, intersections)
classify_distance(distance, len(intersections))
############################################################################################################
# d)
#   Which hyperplane best classifies the training set?
#   First let's find the accuracy of each equation's classification compared to the ground truth, that is
#   compared to the 'gender' column of the training set.  We already know, based on the plot, that 
#   -12.25*x + 2170.21 is the best line. Let's verify this by finding the accuracy of the classifications
#   params:
#       data: data as pandas dataframe
#       classified: data that contains labels 'male'/'female' classifications
#       intersections: intersections for mx+b formula, this equates to the number of hyperplanes
############################################################################################################
def calc_accuracy(data, classified, intersections):
    total_classified    = len(classified[0])
    accuracy            = []
    errors              = []
    for i in range(len(intersections)):
        correct_classified  = 0
        error_classified    = 0
        for j in range(total_classified):
            # Correct classification first
            if data['gender'][j] == 1 and classified[i][j] == 'male':
                correct_classified += 1
            elif data['gender'][j] == -1 and classified[i][j] == 'female':
                correct_classified += 1
            # Erroneous classification
            # Should be female but classified as male
            elif data['gender'][j] == -1 and classified[i][j] == 'male':
                error_classified += 1
            # Should be male but classified as femal
            elif data['gender'][j] == 1 and classified[i][j] == 'female':
                error_classified += 1
            # We should not go here
            else:
                error_classified += 1
        # Calculate accuracy
        acc = (correct_classified / total_classified)*100
        accuracy.append(acc)
        errors.append(error_classified)
        
    print("ACCURACY")
    for i in range(len(intersections)):
        label = str(slope) + '*x + ' + str(intersections[i])
        print("for {} is {:.2f}%".format(label, accuracy[i]))
'''
ANSWER 4d
'''
print("[TRAINING DATA]")
classified = classify_distance(calc_dist_to_hp(data, slope, intersections), len(intersections))
calc_accuracy(data, classified, intersections)
'''
[TRAINING DATA]
ACCURACY
for -12.25*x + 2191.49 is 79.74%
for -12.25*x + 2170.21 is 93.53%
for -12.25*x + 2148.94 is 89.66%
for -12.25*x + 2127.66 is 78.88%
for -12.25*x + 2106.38 is 66.38%

The results prove that the 2nd equation has the best accuracy
'''
############################################################################################################
# e)
#   With the hyperplane selected in the item (d), classify the test set(“DWH Test.csv”). What was
#   the accuracy? Proportionally, is it better or worse than the accuracy found for the training set?
############################################################################################################
# Read CSV file
data = pd.read_csv('../data_sets/DWH_test.csv', delimiter=',', 
                    names=['index', 'height', 'weight', 'gender', 'unknown'])
del data['index']
del data['unknown']
'''
ANSWER 4e
'''
print("[TEST DATA]")
classified = classify_distance(calc_dist_to_hp(data, slope, intersections), len(intersections))
calc_accuracy(data, classified, intersections)
'''
[TEST DATA]
ACCURACY
for -12.25*x + 2191.49 is 73.33%
for -12.25*x + 2170.21 is 86.67%
for -12.25*x + 2148.94 is 82.22%
for -12.25*x + 2127.66 is 66.67%
for -12.25*x + 2106.38 is 53.33%

Hyperplane 2 is slighlty worse than the training set
'''
