#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import math

# Read CSV files
training_data = pd.read_csv('../data_sets/DWH_Training.csv', delimiter=',', 
                        names=['index', 'height', 'weight', 'gender'])
del training_data['index']
test_data = pd.read_csv('../data_sets/DWH_test.csv', delimiter=',', 
                        names=['index', 'height', 'weight', 'gender', 'unknown'])
del test_data['index']
del test_data['unknown']
############################################################################################################
# a)
#   Make a function to calculate the centroids.
############################################################################################################
def compute_centroid(data):
    # Get male and female x/y values
    male      = data.loc[training_data['gender'] == 1]
    female    = data.loc[training_data['gender'] == -1]
    # Compute averges
    male = np.mean(male)
    female = np.mean(female)
    # No longer needed
    del male['gender']
    del female['gender']
    # Return centroids as list
    # male/female[0] = average for height
    # male/female[1] = average for weight
    return [male,female]
    
MALE        = 0
FEMALE      = 1
HEIGHT_X    = 0
WEIGHT_Y    = 1
training_centroid = compute_centroid(training_data)
print("Training Centroid:")
print("  Male:   {:.2f} cm, {:.2f} kg".format(training_centroid[MALE][HEIGHT_X], training_centroid[MALE][WEIGHT_Y]))
print("  Female: {:.2f} cm, {:.2f} kg".format(training_centroid[FEMALE][HEIGHT_X], training_centroid[FEMALE][WEIGHT_Y]))
'''
Answer 3.a
Training Centroid:
  Male:   168.06 cm, 61.11 kg
  Female: 173.60 cm, 69.42 kg
'''
############################################################################################################
# b)
#   Then use these centroids to calculate the linear classifier f (x) = (wT*x + b).
############################################################################################################
def calc_linear_classifier(center_m, center_f,x_data):
    x = 0
    y = 1
    A = 2*(center_f[x] - center_m[x])
    B = 2*(center_f[y] - center_m[y])
    C = center_m[x]**2 + center_m[y]**2 - center_f[x]**2 - center_f[y]**2
    slope = -(A/B)
    inter = -(C/B)
    label = "{:.2f}*x + {:.2f}".format(slope,inter)
    print("Linear Classifier: {}".format(label))
    return [(slope*x_data + inter),label]
    
MALE        = 0
FEMALE      = 1
centers = compute_centroid(training_data)
classifier = calc_linear_classifier(centers[MALE], centers[FEMALE],training_data['height'].values)
'''
Answer 3.b
Linear Classifier: -0.67*x + 179.20
'''
############################################################################################################
# c)
#   Make a scatter plot where the x-axis is the height of the citizens and the y-axis is the weight
#   of the citizens. The color of the points need to be different for males and females. In the same
#   figure, plot the linear classifier calculated in item (b)
############################################################################################################
def plot(training_data,test_data,classifier):
    # Get male data
    males = training_data.loc[training_data['gender'] == 1]
    male_x_data = males['height']
    male_y_data = males['weight']
    # Get female data
    females = training_data.loc[training_data['gender'] == -1]
    female_x_data = females['height']
    female_y_data = females['weight']
    # Plot scatter
    plt.scatter(male_x_data, male_y_data, color='b', label='male')
    plt.scatter(female_x_data, female_y_data, color='r', label='female')
    # plot classifier
    plt.plot(training_data['height'].values,classifier[0],color='g',label=classifier[1])
    # Labels
    plt.xlabel('Height (cm)')
    plt.ylabel('Weight (kg)')
    plt.title('Disnyland Population Weight and Height by Gender')
    plt.legend(loc='lower left')
    plt.show()
    
''' 
Answer 3.c
'''
plot(training_data,test_data,classifier)   
############################################################################################################
# d)
#   Use the dataset “DWH Training.csv” to train your model and the dataset “DWH Test.csv’ to
#   check the accuracy. Compare the NCC accuracy with the accuracy of the models of Exercises
#   Sheet 1. Attention: you need to code the algorithm. Don’t use any library that implements
#   the Nearest Centroid Classifier.
############################################################################################################ 
def NCC(training_data, test_data):
    MALE        = 0
    FEMALE      = 1
    HEIGHT_X    = 0
    WEIGHT_Y    = 1
    # Compute centroid
    centroids = compute_centroid(training_data)
    # Get classifier
    # Classifer[0], classifer label[1]
    classifier = calc_linear_classifier(centroids[MALE], centroids[FEMALE],training_data['height'].values)
    
    test_x         = test_data['height'].values
    test_y         = test_data['weight'].values
    # Create an array of test.rows by train.rows, so we're calculated the distance
    # between every test point to the training set
    size = test_x.size
    distances   = np.zeros((2, size))

    # Find the distances from test to centroid...
    for i in range(size):
        # ... for male center
        calc_x = (test_x[i] - centroids[MALE][HEIGHT_X])**2
        calc_y = (test_y[i] - centroids[MALE][WEIGHT_Y])**2
        distances[MALE][i] = math.sqrt(calc_x + calc_y)
        # ... for female center
        calc_x = (test_x[i] - centroids[FEMALE][HEIGHT_X])**2
        calc_y = (test_y[i] - centroids[FEMALE][WEIGHT_Y])**2
        distances[FEMALE][i] = math.sqrt(calc_x + calc_y)

    predictions    = []
    # Get compare which distance is smaller and predict
    for i in range(size):
        if distances[MALE][i] <= distances[FEMALE][i]:
            predictions.append("male")
        else:
            predictions.append("female")

    return predictions
    
def calc_accuracy(data, classified):
    total_classified    = len(classified)
    correct_classified  = 0
    error_classified    = 0
    data_gender         = data['gender']
    for j in range(total_classified):
        # Correct classification first
        if data_gender[j] == 1 and classified[j] == 'male':
            correct_classified += 1
        elif data_gender[j] == -1 and classified[j] == 'female':
            correct_classified += 1
        # Erroneous classification
        # Should be female but classified as male
        elif data_gender[j] == -1 and classified[j] == 'male':
            error_classified += 1
        # Should be male but classified as femal
        elif data_gender[j] == 1 and classified[j] == 'female':
            error_classified += 1
        # We should not go here
        else:
            error_classified += 1
    # Calculate accuracy
    acc = (correct_classified / total_classified)*100
    print("Correct:   {}".format(correct_classified))
    print("Incorrect: {}".format(error_classified))
    return acc
# Run the code
predictions = NCC(training_data, test_data)
# Find accuracy
acc = calc_accuracy(test_data, predictions)
print("Average Accuracy of NCC: {:.2f}%".format(acc))
'''
Average Accuracy of NCC: 75.56%
'''
