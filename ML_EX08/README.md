# Exercise 8

### Folder structure
Each exercise will contain the following structure:  
```
├── data_sets     #   Contains all required datasets for exercise
├── docs          #   Exercise sheet and team solutions PDF
├── figures       #   Snapshots of required figures for reports
├── scripts       #   Code for exercise
└── README.md
```

### Group Members
```
Mauricio Solis [solisjr@rhrk.uni-kl.de]
Shrutika Jain  [jain@rhrk.uni-kl.de]
Tayyaba Seher  [seher@rhrk.uni-kl.de]
```

### Usage
**It is assumed all date sets provided are added by user in data_sets/ (file too large to upload)**

In the scripts/ directory. Run the "python3 ex8.py" script.
