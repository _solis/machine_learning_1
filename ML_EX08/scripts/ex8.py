import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import random
from random import shuffle
import time
'''
Description:
    Extracts the data, one-hots the categorical, normalizes the numerical, randomizes if selected
Parameters:
    Path to data as csv
    shuffle: randomizes data if true
Returns:
    X: all features
    Y: labels
'''
def extract_data(file, shuffle=True):
    rawdata = pd.read_csv(file, delimiter=',')
    # Drop null columns if any
    rawdata.dropna(inplace = True)

    # Get only those features that are CATigorical
    meow = rawdata.select_dtypes(include=['object']).copy()
    
    # Convert date_time column to datetime64
    meow['date_time'] = pd.to_datetime(meow.date_time, utc=True)
    # Seperate date by year, months, day, and hours only
    meow['date_time_year'] = meow['date_time'].dt.year
    meow['date_time_month'] = meow['date_time'].dt.month
    meow['date_time_day'] = meow['date_time'].dt.day
    meow['date_time_hour'] = meow['date_time'].dt.hour

    # One-Hot the data
    meow = pd.get_dummies(meow)
    meow = meow.select_dtypes(exclude=['object'])
    del meow['date_time'] # probably not needed anymore #YOLO
    
    # Now normalize each column, but first remove the CATegories
    numNums = rawdata.select_dtypes(exclude=['object']).copy()
    # save the columns names, we will need them spater
    columns = numNums.columns
    X = numNums.values
    xmean = np.mean(X, axis=0)
    xstd  = np.std(X, axis=0)
    X = X-xmean
    X = X/xstd
    numNums = pd.DataFrame(data=X, columns=columns)
    
    # Now kiss, combine meow and numNums
    meow.reset_index(drop=True, inplace=True)
    numNums.reset_index(drop=True, inplace=True)
    processed_data = pd.concat([meow, numNums], sort=False, axis=1)

    # randomize
    if(shuffle==True):processed_data = processed_data.sample(frac=1).reset_index(drop=True)

    # freaking finally!
    Y = processed_data['traffic_volume'].values
    del processed_data['traffic_volume']
    X = processed_data.values
    
    return X,Y
'''
Description:
    Splits data based in params
Parameters:
    X: features
    Y: labels
    training_split: percentage of training split, [0.0 to 1.0]
Returns:
    Split data set
'''
def split_data(X, Y, training_split):
    split = int(training_split*len(X))
    train_x = X[:split]
    train_y = Y[:split]
    test_x  = X[split:]
    test_y  = Y[split:]
    
    return train_x, train_y, test_x, test_y
'''
Description:
    Computes [XX^T + (1/2C)I]^-1 = A^-1
Parameters:
    X: features
    C: regularization parameter
Returns:
    A^-1
'''
def compute_Ainv(X, C):
    A = np.dot(X.T, X)
    I = np.identity(X.T.shape[0])
    I = (1 / (2*C))*I
    A += I
    A_inv = np.linalg.inv(A)
    return A_inv
'''
Description:
    Computes A^-1 * Xy
Parameters:
    A_inv: inverse of A
    X: features
    Y: expected value
Returns:
    A^-1 * Xy
'''
def compute_wRR(A_inv, X, Y):
    Y = Y.reshape((Y.shape[0], 1))   
    wRR = np.dot(X.T, Y)
    wRR = np.dot(A_inv, wRR)
    return wRR
'''
Description:
    predicts all values in X against trained wRR
Parameters:
    wRR: hyperplane param
    X: features
Returns:
    y_hat: predictions
'''
def predict(wRR, X):
    y_hat = np.dot(X, wRR)
    return y_hat
'''
Description:
    Leave one point out for testing and use all others for training
Parameters:
    trData: training data as a list containing X[0],Y[1]
    tsData: test data as a list containing X[0],Y[1]
    C: list of regularization parameters to try
Returns:
    
'''
def LOOCV(trData, tsData, C):
    # used for indexes for lists
    X = 0
    Y = 1
    
    # calculate A_inv
    A_inv = compute_Ainv(trData[X], C)
    # calculate wRR
    wRR = compute_wRR(A_inv, trData[X], trData[Y])
    
    # calculate RSME
    serr = 0
    for i in range(0, len(trData[X].shape)):
        K = np.dot(tsData[X][i].T, wRR) - tsData[Y][i]
        L = np.dot(tsData[X][i], A_inv)
        M = 1 - np.dot(L, tsData[X][i].T)
        error = (np.divide(K,M))**2
        serr += error
    RSME = (np.sqrt(serr/len(trData[X].shape)))
    return RSME
'''
    MAIN
'''
if __name__ == "__main__":
    path = '../data_sets/Metro_Interstate_Traffic_Volume.csv'
    # Extract data, onehot catigorical, normalize numerical, and just have fun with it you know?
    X,Y = extract_data(path, shuffle=False)
    trX,trY,tsX,tsY = split_data(X, Y, 0.95)
    C = [0.01, 0.1, 1.0, 10.0, 100.0]
    training_data = [trX,trY]
    testing_data  = [tsX,tsY]
    RSMEs = []
    atleast = 10
    for i,c in enumerate(C):
        tot = 0
        for _ in range(atleast):
            tot += LOOCV(training_data, testing_data, c)
        RSMEs.append(tot/atleast)
    for i,c in enumerate(C):
        print("C = {}".format(c))
        print("   RSME = {:.5f}".format(RSMEs[i][0]))
        
    A_inv = compute_Ainv(testing_data[0], 0.1)
    wRR   = compute_wRR(A_inv, testing_data[0], testing_data[1])
    y_hat = predict(wRR, testing_data[0])
    
    testing_data[1] = testing_data[1].reshape((testing_data[1].shape[0], 1)) 
    d1 = y_hat - testing_data[1]
    d2 = (y_hat - testing_data[1])**2

    fig, ax = plt.subplots()
    plotdata = [d1.flatten(), d2.flatten()] 
    boxplots = ax.boxplot(plotdata,
               notch = True,
               labels=['d1', 'd2'],
               widths = .7,
               patch_artist=True,
               medianprops = dict(linestyle='-', linewidth=2, color='Yellow'),
               boxprops = dict(linestyle='--', linewidth=2, color='Black', facecolor = 'blue', alpha = .4)
              )

    boxplot1 = boxplots['boxes'][0]
    boxplot1.set_facecolor('red')

    plt.ylabel('deviations', fontsize = 14)
    plt.xticks(fontsize = 14)
    plt.yticks(fontsize = 14)
    plt.show()
'''
C = 0.01
   RSME = 0.63026
C = 0.1
   RSME = 0.62985
C = 1.0
   RSME = 0.62992
C = 10.0
   RSME = 0.63023
C = 100.0
   RSME = 0.63148
'''

