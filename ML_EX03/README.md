# Exercise 3

### Folder structure
Each exercise will contain the following structure:  
```
├── data_sets     #   Contains all required datasets for exercise
├── docs          #   Exercise sheet and team solutions PDF
├── figures       #   Snapshots of required figures for reports
├── scripts       #   Code for exercise
└── README.md
```

### Group Members
```
Mauricio Solis [solisjr@rhrk.uni-kl.de]
Shrutika Jain  [jain@rhrk.uni-kl.de]
Tayyaba Seher  [seher@rhrk.uni-kl.de]
```

### Usage
In the scripts/ directory. Run the "ex3_run.sh" script.

It is assumed that it will be running on a bash shell, that Python 2 and 3 are installed on the running computer, and that LIBLINEAR and vowpal wabbit are also installed.

### Notes
phraug2-master was used to convert both from CSV to LIBSVM and vowpal wabbit formats.  Modifactions had to be done to both scripts as they would not run "out of the box" for given assignment files. 

For VW, careful consideration of desired format had to be taken.  Desired output must be "[LABEL] [TAG]|Namespace [FEATURES]. Where, LABEL is in column 94 for training and no label was given for test data TAG is, "ex[ID]". ID is in column 0, Namespace is "f", and each column is considered a feature, where a feature with 0 was dropped.

For LIBSVM, format is as follows [label] [index]:[value1] [index2]:[value2] ...
