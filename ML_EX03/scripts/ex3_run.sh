echo "Converting DWH training and test data to LIBSVM format..."
python ../../Resources/phraug2-master/csv2libsvm.py ../data_sets/dwh/DWH_Training.csv ../data_sets/dwh/DWH_Training.svm -l3 -c0
python ../../Resources/phraug2-master/csv2libsvm.py ../data_sets/dwh/DWH_test.csv ../data_sets/dwh/DWH_test.svm -l3 -c0 -c3
echo "...done"

echo "Running Task 1..."
python3 ex3_task1.py
echo "... done"

echo "Converting INS training data to VW format..."
python ../../Resources/phraug2-master/csv2vw.py ../data_sets/ins/INS_training.csv ../data_sets/ins/INS_training.vw -l94 -n10000
echo "Converting INS test data to VW format..."
python ../../Resources/phraug2-master/csv2vw.py ../data_sets/ins/INS_test.csv ../data_sets/ins/INS_test.vw -l-1 -n1000
echo "...done"

echo "Running Task 2..."
echo "************ TRAINING ************"
vw -d ../data_sets/ins/INS_training.vw --oaa 9 -f ../data_sets/ins/ins.model --cache_file ../data_sets/ins/cache_file --loss_function logistic --passes 20
echo "************ TESTING ************"
vw -d ../data_sets/ins/INS_training.vw -t -i ../data_sets/ins/ins.model -p ../data_sets/ins/ins_pred.txt
echo "... done"
