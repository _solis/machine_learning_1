# Exercise 6

### Folder structure
Each exercise will contain the following structure:  
```
├── data_sets     #   Contains all required datasets for exercise
├── docs          #   Exercise sheet and team solutions PDF
├── figures       #   Snapshots of required figures for reports
├── scripts       #   Code for exercise
└── README.md
```

### Group Members
```
Mauricio Solis [solisjr@rhrk.uni-kl.de]
Shrutika Jain  [jain@rhrk.uni-kl.de]
Tayyaba Seher  [seher@rhrk.uni-kl.de]
```

### Usage
In the scripts/ directory. Run the "python3 ex6.py" script.

It is assumed that it will be running on a bash shell, that Python 2 and 3 are installed on the running computer.
