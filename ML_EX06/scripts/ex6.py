import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import random
from random import shuffle

INPUT_LAYER_SIZE    = 2 # Number of attributes
HIDDEN_LAYERS       = 2
HIDDEN_LAYER_SIZE   = 5
OUTPUT_LAYER_SIZE   = 1
DATA_SPLIT          = 95 # Training data split
SGD_ITERATIONS      = 10000
SGD_BATCH_SIZE      = 50
SGD_EVALUATE        = 250 # iteration where SGD will evaluate
PREV_LOSS           = 5 # used to check validation loss > PREV_LOSS[all]
# For debugging
PRINT_MATRICES      = 0
PRINT_MAT_SHAPES    = 0
'''
Description:
    Extracts the data as a list of lists of strings and normalizes from 0 to 1
Parameters:
    Path to data as csv
'''
def extract_data(file):
    rawdata = pd.read_csv(file, delimiter=',',
            names=['index', 'height', 'weight', 'gender'])
    # Not needed
    del rawdata['index']
    # Drop null columns if any
    rawdata.dropna(inplace = True)
    # Replace all -1 to 0
    rawdata.loc[rawdata['gender'] == -1, 'gender'] = 0
    # Normalize
    rawdata['height'] = ((rawdata['height']-rawdata['height'].min())/(rawdata['height'].max()-rawdata['height'].min()))
    rawdata['weight'] = ((rawdata['weight']-rawdata['weight'].min())/(rawdata['weight'].max()-rawdata['weight'].min()))
    data = []
    for i in range(len(rawdata.index)):
        point = dict()
        point['po'] = np.array([float(rawdata.loc[i].height), float(rawdata.loc[i].weight)])
        point['cl'] = int(rawdata.loc[i].gender)
        data.append(point)
    return data
'''
Description:
    Randomly initializes weight matrices. 
    Distribution with mean 0 and variance 1
'''
def init_weights():
    W_mat = []
    # Do input to first hidden
    # rows are the number of attributes and columns refer to neurons in next layer
    r = np.sqrt(2.0/HIDDEN_LAYER_SIZE)
    W_mat.append(np.random.randn(INPUT_LAYER_SIZE, HIDDEN_LAYER_SIZE) * r)
    # Do all hidden layers
    for i in range(HIDDEN_LAYERS-1):
        W_mat.append(np.random.randn(HIDDEN_LAYER_SIZE, HIDDEN_LAYER_SIZE) * r)
    # Do output layer
    r = np.sqrt(2.0/OUTPUT_LAYER_SIZE)
    W_mat.append(np.random.randn(HIDDEN_LAYER_SIZE, OUTPUT_LAYER_SIZE) * r)
    
    if(PRINT_MAT_SHAPES == 1):
        print("\nSynapse Matrices:")
        print("   Input to Hidden: {} ".format(W_mat[0].shape))
        if (PRINT_MATRICES == 1):print(W_mat[0])
        print("   Hidden to Hidden:")
        for i in range(1, len(W_mat)-1):
            print("      W[{}]: {} ".format(i, W_mat[i].shape))
            if (PRINT_MATRICES == 1):print(W_mat[i])
        print("   Hidden to Output: {} ".format(W_mat[HIDDEN_LAYERS].shape))
        if (PRINT_MATRICES == 1):print(W_mat[HIDDEN_LAYERS])
    return W_mat
'''
Description:
    Initialize bias terms. Bias is attached to neurons and added to the weighted input before the activation function is applied. This allows us to shift our neuron’s activation outputs left and right. This helps us model datasets that do not necessarily pass through the origin
    Each neuron has its own bias constant
'''
def init_bias():
    B_mat = []
    # Initialize hidden layer bias
    r = np.sqrt((2/HIDDEN_LAYER_SIZE))
    for i in range(HIDDEN_LAYERS):
        B_mat.append(np.random.randn(1, HIDDEN_LAYER_SIZE) * r)
    # Init output layer bias
    r = np.sqrt((2/OUTPUT_LAYER_SIZE))
    B_mat.append(np.random.randn(1, OUTPUT_LAYER_SIZE) * r)
    
    if(PRINT_MAT_SHAPES == 1):
        print("\nBias Matrices:")
        print("   Input to Hidden: {} ".format(B_mat[0].shape))
        if (PRINT_MATRICES == 1):print(B_mat[0])
        print("   Hidden:")
        for i in range(1, len(B_mat)-1):
            print("      B[{}]: {} ".format(i, B_mat[i].shape))
            if (PRINT_MATRICES == 1):print(B_mat[i])
        print("   Hidden to Output: {} ".format(B_mat[HIDDEN_LAYERS].shape))
        if (PRINT_MATRICES == 1):print(B_mat[HIDDEN_LAYERS])
    
    return B_mat
'''
Description:
    Returns a batch of random samples
Parameters:
    X = features
    Y = ground truth labels
    s = total length of samples
'''
def next_batch(X, Y, s):
    batch = np.random.randint(0, s, size=SGD_BATCH_SIZE)
    return X[batch,:],Y[batch]
'''
Description:
    Returns the classes and accuracy for correct probabilities
Parameters:
    Y = ground truth labels
    Yhat = results of output layer
'''
def accuracy(Y, Yhat):
    cl = np.full(Yhat.shape, 0)
    cl[Yhat >= 0.5] = 1
    correct = (cl == Y).mean()
    return cl, correct
'''
Description:
    Holds W and B matrices of trained model
'''
class Model(object):  
  def __init__(self, W, B):
    self.W = W
    self.B = B
  def set_params(self, W,B):
    self.W = W
    self.B = B
'''
Description:
    Trains based on X data
Parameters:
    data = data as pandas dataframe
    W = synapse weight matrices
    B = bias matrices
    act_f = activation function
    act_prime = activation function derivative
    cost_f = cost function
    cost_prime = cost function derivative
    model = model object
    lr = learning rate
'''
def SGD(data, W, B, act_f, act_prime, cost_f, cost_prime, model, lr):
    train_loss = []
    train_acc  = []
    val_loss   = []
    val_acc    = []
    models     = []
    previous_losses = []
    # Shuffle and split the data
    tr_x,tr_y,val_x,val_y = split_data(data, DATA_SPLIT)
    num_samples = len(tr_x)
    # SGD
    for t in range(1, SGD_ITERATIONS+1):
        # Get our batch generator and loop over them
        batch_x, batch_y = next_batch(tr_x, tr_y, num_samples)
        
        # Evaluate our gradient on the batch
        Z,H,Yhat = feed_forward(batch_x, W, B, act_f)
        
        # get the loss for probabilities
        t_loss = cost_f(batch_y, Yhat)
        
        # Get the accuracy for correct probabilities
        _,t_acc = accuracy(batch_y, Yhat)
        
        # backprop to find dw,db
        dw,db = backprop(W, B, batch_x, batch_y, Yhat, H, Z, act_prime, cost_prime)
        # This part takes the gradient step
        # Update our hyperplane parameters
        for l in range(HIDDEN_LAYERS+1):
            W[l] -= (lr/t)*dw[l]
            B[l] -= (lr/t)*db[l]
        # Validate loss and accuracy on certain iterations
        if (t%SGD_EVALUATE == 0):
            # set new params for this model
            model.set_params(W,B)
            # validate
            _,_,val_Yhat = feed_forward(val_x, model.W, model.B, act_f)
            v_loss = cost_f(val_y, val_Yhat)
            _,v_acc  = accuracy(val_y, val_Yhat)

            # check for previous validation losses
            if(len(val_loss) == PREV_LOSS):
                for i,prev_loss in enumerate(val_loss):
                    # if v_loss is greater than any previously validated, stop SGD and return our last 5 models
                    if(v_loss > prev_loss):
                        # Return the index of the best model with best training results
                        best_l      = 1000000.00
                        best_i      = 0
                        for ii in range(len(train_loss)):
                            if (train_loss[ii] < best_l):
                                best_l = train_loss[ii]
                                best_i = ii
                        return train_loss,train_acc,val_loss,val_acc,models,best_i
                                
                # clear all elements after check
                train_loss = []
                train_acc  = []
                val_loss   = []
                val_acc    = []
                models     = []
            else:
                # append them to our list
                models.append(model)
                train_loss.append(t_loss)
                train_acc.append(t_acc)
                val_loss.append(v_loss)
                val_acc.append(v_acc)
    # Return the index of the best model with best training results
    best_l      = 1000000.00
    best_i      = 0
    for v in range(len(train_loss)):
        if (train_loss[v] < best_l):
            best_l = train_loss[v]
            best_i = v
    return train_loss,train_acc,val_loss,val_acc,models,best_i
'''
Description:
    Sigmoid neuron activation function and its derivative
Params:
    x = weighted input matrix to the neuron
'''
def sigmoid(x):
    return 1.0 / (1 + np.exp(-x))
def sigmoid_prime(x):
    sp = sigmoid(x)
    return sp*(1-sp)
'''
Description:
    ReLU neuron activation function and its derivative
Params:
    x = weighted input matrix to the neuron
'''
def relu(x):
    return np.maximum(0, x)
def relu_prime(x):
    x[x < 0] = 0
    x[x > 0] = 1
    return x
'''
Description:
    Binary cross entropy cost function and its derivative
Params:
    Y = matrix for ground truth, the labels
    Yhat = matrix for results of output layer, the predictions
'''
def cross_entropy(Y, Yhat):
    cost = -(np.dot(Y.T, np.log(Yhat)) + np.dot((1 - Y.T), np.log(1 - Yhat)))/len(Yhat)
    return np.squeeze(cost)
def cross_entropy_prime(Y, Yhat):
    cost = -(np.divide(Y, Yhat) - np.divide((1 - Y), (1 - Yhat)))
    return cost
'''
Description:
    Feed-forward propagation
Params:
    X = input matrix
    W = weights for synapses as matrix
    B = bias for each neuron as matrix
    f = activation function
Returns:
    Z = list of INPUT dot W
    H = list of activation_function(Z)
    Yhat = results of output layer
'''
def feed_forward(X, W, B, f):
    # Input layer to first hidden
    # X dot W + B, input dot weights to hidden layer + bias = Z
    Z = []
    H = []
    Z.append(np.dot(X, W[0]) + B[0])
    #Z.append(np.dot(X, W[0]) + B[0])
    # H = Activation Function(Z)
    H.append(f(Z[0]))
    # Hidden layers, H dot W, hidden activation dot weights to next hidden layers + bias
    for i in range(1, HIDDEN_LAYERS):
        # prev. activations dot weights + bias
        Z.append(np.dot(H[i-1], W[i]) + B[i])
        H.append(f(Z[i]))
    # Output layer
    Z.append(np.dot(H[-1], W[-1]) + B[-1])
    Yhat = f(Z[-1])

    # DEBUG
    if(PRINT_MAT_SHAPES == 1):
        print("\nInput Weighted Matrices (Z):")
        print("   Input to Hidden: {} ".format(Z[0].shape))
        if (PRINT_MATRICES == 1):print(Z[0])
        print("   Hidden to Hidden:")
        for i in range(1, len(Z)-1):
            print("      Z[{}]: {} ".format(i, Z[i].shape))
            if (PRINT_MATRICES == 1):print(Z[i])
        print("   Hidden to Output: {} ".format(Z[HIDDEN_LAYERS].shape))
        if (PRINT_MATRICES == 1):print(Z[HIDDEN_LAYERS])
        print("\nHidden Activation Matrices (H):")
        for i in range(len(H)):
            print("   H[{}]: {} ".format(i, H[i].shape))
            if (PRINT_MATRICES == 1):print(H[i])
        print("\nOutput Layer Matrix: {}".format(Yhat.shape))
        if (PRINT_MATRICES == 1):print(Yhat)

    # Return results of output layer
    return Z,H,Yhat
'''
Description:
    Backpropagation
Params:
    W = weight matrices
    B = bias matrices
    X = input data
    Y = labels for all samples
    Yhat = probability from feed-forward
    H = hidden layers after activation function
    Z = weighted input to hidden layers, before activation
    a_prime = derivative of activation function
    cost_prime = derivative of cost function
Returns:
    db = layer error / deriviative of cost with respect to bias
    dw = derivative of cost with respect to weight
'''
def backprop(W, B, X, Y, Yhat, H, Z, a_prime, cost_prime):
    # First calculate the output layer error
    # We need to find the derivate of cost with respect to the output layer input Z[HIDDEN_LAYERS]
    # It answers, how are the final layer's weights impacting overall error in the network?
    # So, Eo = C'(yhat) * ActivationFunction'(Zo)
    # Error in nodes in l-th layer
    E = [0] * (HIDDEN_LAYERS+1)
    # Derivative of cost with respect to any weight
    C = [0] * (HIDDEN_LAYERS+1)
    db = [0] * (HIDDEN_LAYERS+1)
    # Last layer
    E[-1] = cost_prime(Y, Yhat) * a_prime(Z[-1])
    # To find the derivative of cost with respect to any weight, multiply current layer error * activation
    input_size = len(H[-1])
    C[-1] = np.dot(H[-1].T, E[-1])/input_size
    db[-1] = np.sum(E[-1], axis=0, keepdims=True)/input_size
    # Calculate the current layer's error and pass the weighted error
    # back to the previous layer until first hidden layer
    for i in range(HIDDEN_LAYERS-1, -1, -1):
        E[i] = np.dot(E[i+1], W[i+1].T) * a_prime(Z[i])
        input_size = len(H[i])
        # Input to first hidden layer is X
        if (i != 0):
            C[i] = np.dot(H[i].T, E[i])/input_size
            db[i] = np.sum(E[i], axis=0, keepdims=True)/input_size
    # Input to first hidden layer
    input_size = len(X)
    C[0] = np.dot(X.T, E[0])/input_size
    db[0] = np.sum(E[0], axis=0, keepdims=True)/input_size
    # DEBUG
    if(PRINT_MAT_SHAPES == 1):
        print("\nError Matrices:")
        print("   Output Layer: {}".format(E[-1].shape))
        if (PRINT_MATRICES == 1):print(E[-1])
        for i in range(len(E)-2, -1, -1):
            print("   Hidden Layer {}: {}".format(i+1, E[i].shape))
            if (PRINT_MATRICES == 1):print(E[i])
        print("\ndb Matrices:")
        print("   Output Layer: {}".format(db[-1].shape))
        if (PRINT_MATRICES == 1):print(db[-1])
        for i in range(len(db)-2, -1, -1):
            print("   Hidden Layer {}: {}".format(i+1, db[i].shape))
            if (PRINT_MATRICES == 1):print(db[i])
        print("\ndw Matrices:")
        print("   Last Hidden to Output: {}".format(C[-1].shape))
        if (PRINT_MATRICES == 1):print(C[-1])
        for i in range(len(C)-2, 0, -1):
            print("   Hidden_{} to Hidden_{}: {}".format(i, i+1, C[i].shape))
            if (PRINT_MATRICES == 1):print(C[i])
        print("   Input to Hidden_1: {}".format(C[0].shape))
        if (PRINT_MATRICES == 1):print(C[0])
    dw = C
    return dw,db
'''
Description:
    Splits data based in params
Parameters:
    data  = input data
    split = percentage of training split, [0 to 100]
Returns:
    Split data set
'''
def split_data(data, split):
    random.shuffle(data)
    size  = len(data)
    tr_p  = (split/100)*size
    train = np.array(data[:int(tr_p)])    # from start to tr_p
    test  = np.array(data[int(tr_p):])    # from tr_p to end
    train_x = np.array([point['po'] for point in train], dtype = np.float64)
    train_y = np.array([point['cl'] for point in train], dtype = np.float64)
    test_x = np.array([point['po'] for point in test], dtype = np.float64)
    test_y = np.array([point['cl'] for point in test], dtype = np.float64)
    # Reshape, intially this ends up (n,)
    train_y = train_y.reshape((train_y.shape[0], 1))
    test_y  = test_y.reshape((test_y.shape[0], 1))
    
    return train_x, train_y, test_x, test_y
'''
    MAIN
'''
if __name__ == "__main__":
    # Extract data and change the -1 to a 0
    data = extract_data('../data_sets/DWH_Training.csv')
    X = np.array([point['po'] for point in data], dtype = np.float64)
    Y = np.array([point['cl'] for point in data], dtype = np.float64)
    Y = Y.reshape((Y.shape[0], 1))
    if(PRINT_MAT_SHAPES == 1):
        print("Input Matrix: {}".format(X.shape))
    if (PRINT_MATRICES == 1):print(X)
    # Initialize hyperplane params
    W = init_weights()
    B = init_bias()
    model = Model(W,B)
    train_loss,train_acc,val_loss,val_acc, trained_model, best_i = \
        SGD(data, W, B,                     \
        sigmoid, sigmoid_prime,             \
        cross_entropy, cross_entropy_prime, \
        model, 5)
        
    epochs = range(SGD_EVALUATE,SGD_EVALUATE*(len(val_loss)+1),SGD_EVALUATE)
    plt.plot(epochs,train_acc,'b',label='Training Acc')
    plt.plot(epochs,val_acc,'r',label='Validation Acc')
    
    plt.title('Training and Validation Accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()

    plt.show()
    
    # 3
    lmda   = []
    losses = []
    models = []
    all_best_losses = []
    for i in np.arange(1,21,1):
        train_loss,train_acc,val_loss,val_acc, trained_model, best_i = \
            SGD(data, W, B,                     \
            sigmoid, sigmoid_prime,             \
            cross_entropy, cross_entropy_prime, \
            model, i)
            
        lmda.append(i)
        losses.append(val_loss[-1])
        all_best_losses.append(val_loss[best_i])
        models.append(trained_model[best_i])
        
    # Plot lamda vs final validation error
    plt.plot(lmda, losses,'r',label='Validation Loss')
    plt.title('Lambda and Validation Loss')
    plt.xlabel('Lambda')
    plt.ylabel('Loss')
    plt.legend()
    plt.show()
    
    # find best model
    best_model  = model
    best_l      = 1000000.00
    bestest     = 0
    for i in range(len(lmda)):
        for i in range(len(all_best_losses)):
            if (all_best_losses[i] < best_l):
                best_l = all_best_losses[i]
                bestest = i
    best_model = models[bestest]
    best_lamda = lmda[bestest]
    
    # Test, for some reasone DWH_test has some dumb column, so delete it and rewrite
    data = pd.read_csv('../data_sets/DWH_test.csv', delimiter=',', 
                    names=['index', 'height', 'weight', 'gender', 'unknown'])
    del data['unknown']
    data.to_csv(path_or_buf='../data_sets/DWH_Test.csv', index=False, header=False)
    
    data = extract_data('../data_sets/DWH_Test.csv')
    X = np.array([point['po'] for point in data], dtype = np.float64)
    Y = np.array([point['cl'] for point in data], dtype = np.float64)
    Y = Y.reshape((Y.shape[0], 1))
    
    _,_,val_Yhat    = feed_forward(X, best_model.W, best_model.B, sigmoid)
    v_loss          = cross_entropy(Y, val_Yhat)
    _,v_acc         = accuracy(Y, val_Yhat)
    
    print("Best learning rate is {}/t".format(best_lamda))
    print("Accuracy : {:.2f}%".format(v_acc*100))
    
