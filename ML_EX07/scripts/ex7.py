import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import random
from random import shuffle

INPUT_LAYER_SIZE    = 2 # Number of attributes
HIDDEN_LAYERS       = 2
HIDDEN_LAYER_SIZE   = 5
OUTPUT_LAYER_SIZE   = 1
SPLIT      = 0.95 # Training data split
ITERS      = 1009
BATCH      = 50
EVAL       = 25 # iteration where SGD will evaluate
'''
Description:
    Extracts the data as a list of lists of strings and normalizes from 0 to 1
Parameters:
    Path to data as csv
    shuffle: randomizes data
'''
def extract_data(file, shuffle=True):
    rawdata = pd.read_csv(file, delimiter=',',
            names=['index', 'height', 'weight', 'gender'])
    # Not needed
    del rawdata['index']
    # Drop null columns if any
    rawdata.dropna(inplace = True)
    # Replace all -1 to 0
    rawdata.loc[rawdata['gender'] == -1, 'gender'] = 0
    data = []
    for i in range(len(rawdata.index)):
        point = dict()
        point['po'] = np.array([float(rawdata.loc[i].height), float(rawdata.loc[i].weight)])
        point['cl'] = int(rawdata.loc[i].gender)
        data.append(point)
    # randomize
    if(shuffle==True):random.shuffle(data)
    # normaliize    
    X = np.array([point['po'] for point in data], dtype = np.float64)
    xmean = np.mean(X, axis=0)
    xstd  = np.std(X, axis=0)
    X = X-xmean
    X = X/xstd
    Y = np.array([point['cl'] for point in data], dtype = np.float64)
    Y = Y.reshape((Y.shape[0], 1))
    
    return X,Y
'''
Description:
    Sigmoid neuron activation function and its derivative
Params:
    x = weighted input matrix to the neuron
'''
def sigmoid(x):
    return 1.0 / (1 + np.exp(-x))
def sigmoid_prime(x):
    sp = sigmoid(x)
    return np.multiply(sp, (1-sp))
'''
Description:
    ReLU neuron activation function and its derivative
Params:
    x = weighted input matrix to the neuron
'''
def relu(x):
    return np.maximum(0, x)
def relu_prime(x):
    x[x < 0] = 0
    x[x > 0] = 1
    return x
'''
Description:
    Binary cross entropy cost function and its derivative
Params:
    Y = matrix for ground truth, the labels
    Yhat = matrix for results of output layer, the predictions
'''
def cross_entropy(Y, Yhat):
    cost = -(np.dot(Y, np.log(Yhat)) + np.dot((1 - Y), np.log(1 - Yhat)))/len(Yhat)
    return np.squeeze(cost)
def cross_entropy_prime(Y, Yhat):
    cost = -(np.divide(Y, Yhat)) + np.divide((1 - Y), (1 - Yhat))
    return cost
'''
Description:
    Method to initialize parameters based on:
    -sqrt(2/d), sqrt(2/d)
'''
def method1():
    W = []
    B = []
    # Do input to first hidden
    # rows are the number of attributes and columns refer to neurons in next layer
    r = np.sqrt(2.0/HIDDEN_LAYER_SIZE)
    W.append(np.random.randn(INPUT_LAYER_SIZE, HIDDEN_LAYER_SIZE) * r)
    B.append(np.random.randn(1, HIDDEN_LAYER_SIZE) * r)
    # Do all hidden layers
    for i in range(HIDDEN_LAYERS-1):
        W.append(np.random.randn(HIDDEN_LAYER_SIZE, HIDDEN_LAYER_SIZE) * r)
        B.append(np.random.randn(1, HIDDEN_LAYER_SIZE) * r)
    # Do output layer
    r = np.sqrt(2.0/OUTPUT_LAYER_SIZE)
    W.append(np.random.randn(HIDDEN_LAYER_SIZE, OUTPUT_LAYER_SIZE) * r)
    B.append(np.random.randn(1, OUTPUT_LAYER_SIZE) * r)
    return W,B
'''
Description:
    Method to initialize parameters based on:
         uniform distribution [0,1]
'''
def method2():
    W = []
    B = []
   # Do input to first hidden
    # rows are the number of attributes and columns refer to neurons in next layer
    W.append(np.random.rand(INPUT_LAYER_SIZE, HIDDEN_LAYER_SIZE))
    B.append(np.random.rand(1, HIDDEN_LAYER_SIZE))
    # Do all hidden layers
    for i in range(HIDDEN_LAYERS-1):
        W.append(np.random.rand(HIDDEN_LAYER_SIZE, HIDDEN_LAYER_SIZE))
        B.append(np.random.rand(1, HIDDEN_LAYER_SIZE))
    # Do output layer
    W.append(np.random.rand(HIDDEN_LAYER_SIZE, OUTPUT_LAYER_SIZE))
    B.append(np.random.rand(1, OUTPUT_LAYER_SIZE))
    return W,B
'''
Description:
    Method to initialize parameters based on:
         all zeros
'''
def method3():
    W = []
    B = []
   # Do input to first hidden
    # rows are the number of attributes and columns refer to neurons in next layer
    W.append(np.zeros((INPUT_LAYER_SIZE, HIDDEN_LAYER_SIZE)))
    B.append(np.zeros((1, HIDDEN_LAYER_SIZE)))
    # Do all hidden layers
    for i in range(HIDDEN_LAYERS-1):
        W.append(np.zeros((HIDDEN_LAYER_SIZE, HIDDEN_LAYER_SIZE)))
        B.append(np.zeros((1, HIDDEN_LAYER_SIZE)))
    # Do output layer
    W.append(np.zeros((HIDDEN_LAYER_SIZE, OUTPUT_LAYER_SIZE)))
    B.append(np.zeros((1, OUTPUT_LAYER_SIZE)))
    return W,B
'''
Description:
    Method to initialize parameters based on:
         normal ditribution [-10,10]
'''
def method4():
    W = []
    B = []
    W.append(np.random.randn(INPUT_LAYER_SIZE, HIDDEN_LAYER_SIZE) * 10)
    B.append(np.random.randn(1, HIDDEN_LAYER_SIZE) * 10)
    # Do all hidden layers
    for i in range(HIDDEN_LAYERS-1):
        W.append(np.random.randn(HIDDEN_LAYER_SIZE, HIDDEN_LAYER_SIZE) * 10)
        B.append(np.random.randn(1, HIDDEN_LAYER_SIZE) * 10)
    # Do output layer
    r = np.sqrt(2.0/OUTPUT_LAYER_SIZE)
    W.append(np.random.randn(HIDDEN_LAYER_SIZE, OUTPUT_LAYER_SIZE) * 10)
    B.append(np.random.randn(1, OUTPUT_LAYER_SIZE) * 10)
    return W,B
'''
Artificial Neural Network
'''
class ANN(object):
    '''
    Description:
        Randomly initializes weight matrices. Distribution with mean 0 and variance 1
        Initialize bias terms. Bias is attached to neurons and added to the weighted 
        input before the activation function is applied. This allows us to shift our 
        neuron’s activation outputs left and right. This helps us model datasets that 
        do not necessarily pass through the origin. Each neuron has its own bias constant
    '''
    def __init__(self, hidden_layers, init_params, \
                 act_f, act_f_prime, loss_f, loss_f_prime):
        # initialize parameters based on method provided
        self.W,self.B = init_params()
    
        # wehighted input and activation layer
        self.Z = [0] * (hidden_layers+1)
        self.A = [0] * (hidden_layers+1)
        # set functions
        self.act_f = act_f
        self.act_f_prime = act_f_prime
        self.loss_f = loss_f
        self.loss_f_prime = loss_f_prime
        # error
        self.E = [0] * (hidden_layers+1)
        # gradients
        self.dw = [0] * (hidden_layers+1)
        self.db = [0] * (hidden_layers+1)
        # network output
        self.Yhat = [0]
        # network's validation accuracy
        self.acc = 0
    '''
    Description:
        Feed-forward propagation
    Parameters:
        X: features
    Returns:
        Yhat: results of output layer
    '''
    def feed_forward(self, X):
        # Input layer to first hidden
        # X * W + B, input * weights to hidden layer + bias = Z
        self.Z[0] = np.dot(X, self.W[0]) + self.B[0]
        # A = Activation Function(Z)
        self.A[0] = self.act_f(self.Z[0])
        # Hidden layers, A * W, hidden activation * weights to next hidden layers + bias
        for i in range(1, len(self.W)-1):
            # prev. activations * weights + bias
            self.Z[i] = np.dot(self.A[i-1], self.W[i]) + self.B[i]
            self.A[i] = self.act_f(self.Z[i])
        # Output layer
        self.Z[-1] = np.dot(self.A[i], self.W[-1]) + self.B[-1]
        self.Yhat  = self.act_f(self.Z[-1])
        return self.Yhat
    '''
    Description:
        Backpropagation
    Parameters:
        X: features
        Y: labels
    Returns:
        dw: derivative of cost with respect to weight
        db: layer error / deriviative of cost with respect to bias
    '''
    def backprop(self, X, Y):
        layers = len(self.W)
        # First calculate the output layer error
        # We need to find the derivate of cost with respect to the output layer input Z[HIDDEN_LAYERS]
        # It answers, how are the final layer's weights impacting overall error in the network?
        # So, Eo = C'(yhat) * ActivationFunction'(Zo)
        # Error in nodes in l-th layer
        # Derivative of cost with respect to any weight
        # Last layer
        self.E[-1] = np.multiply(self.loss_f_prime(Y, self.Yhat), self.act_f_prime(self.Z[-1]))
        # To find the derivative of cost with respect to any weight, multiply current layer error * activation
        self.dw[-1] = np.outer(self.A[-1], self.E[-1])
        self.db[-1] = np.sum(self.E[-1], axis=0, keepdims=True)
        # Calculate the current layer's error and pass the weighted error
        # back to the previous layer until first hidden layer
        for i in range(layers-2, -1, -1):
            self.E[i] = np.dot(self.E[i+1], self.W[i+1].T) * self.act_f_prime(self.Z[i])
            # Input to first hidden layer is X
            if (i != 0):
                self.dw[i] = np.outer(self.A[i], self.E[i])
                self.db[i] = np.sum(self.E[i], axis=0, keepdims=True)
        # Input to first hidden layer
        self.dw[0] = np.outer(X, self.E[0])
        self.db[0] = np.sum(self.E[0], axis=0, keepdims=True)
        return self.db,self.dw
    '''
    Description:
        Function to take the gradient decent
    Paramters:
        X: features
        Y: labels
        batch_size: SGD batch size
        lr: SGD learning rate
    Returns:
        batch_y: features and labels for data used in step
        loss: loss of probability vs actual label
    '''
    def gradient_step(self, X, Y, batch_size, lr, phi=-1.0):
        layers   = len(self.W)
        self.phi = phi
        # initialize
        dw      = []
        db      = []
        dtw     = []
        dtb     = []
        for i in range(0, layers):
            dw.append(np.zeros(self.W[i].shape))
            dtw.append(np.zeros(self.W[i].shape))
            db.append(np.zeros(self.B[i].shape))
            dtb.append(np.zeros(self.B[i].shape))
        loss    = np.full((batch_size,1), 0)
        # Get our sgd batch
        batch_x, batch_y = self.next_batch(X, Y, batch_size)
        for s in range(0, batch_size):
            # Obtain Yhat
            self.feed_forward(batch_x[s])
            # backprop to find dw,db
            self.backprop(batch_x[s], batch_y[s])
            for i in range(0, layers):
                dw[i] += self.dw[i]
                db[i] += self.db[i]
            # get the loss for probabilities
            loss[s]  = self.loss_f(batch_y[s], self.Yhat)
        # This part takes is the actual gradient step
        # Update our hyperplane parameters
        for i in range(0, layers):
            # mean
            dw[i] = dw[i]*(1.0/batch_size)
            db[i] = db[i]*(1.0/batch_size)
            if (self.phi > 0.0):
                # update momentums
                dtw[i] = (self.phi*dtw[i]) + dw[i]
                dtb[i] = (self.phi*dtb[i]) + db[i]
                # update parameters
                self.W[i] -= lr*dtw[i]
                self.B[i] -= lr*dtb[i]
            else:
                self.W[i] -= lr*dw[i]
                self.B[i] -= lr*db[i]
        return batch_y,loss
    '''
    Description:
        Returns a batch of random samples
    Parameters:
        X: features
        Y: labels
        batch_size: batch size desired
    '''
    def next_batch(self, X, Y, batch_size):
        batch = np.random.randint(0, len(X), size=batch_size)
        return X[batch,:],Y[batch]
    '''
    Description:
        Returns a class of 0 or 1
    Parameters:
        X: features to classify
    Returns:
        predicted classes
    '''
    def classify(self, X):
        # Evaluate test point
        prob = self.feed_forward(X)
        if (prob >= 0.5):return 1
        return 0
    '''
    Description:
        Returns the accuracy of correct predicitons
    Parameters:
        Y: ground truth labels
        pred: predictions
    Returns:
        acc: accuracy
    '''
    def accuracy(self, Y, pred):
        acc = np.sum(np.equal(Y, pred))/Y.shape[0]
        self.acc = acc
        return acc
    '''
    Description:
        Splits data based in params
    Parameters:
        X: features
        Y: labels
        training_split: percentage of training split, [0.0 to 1.0]
    Returns:
        Split data set
    '''
    def split_data(self, X, Y, training_split):
        split = int(training_split*len(X))
        train_x = X[:split]
        train_y = Y[:split]
        test_x  = X[split:]
        test_y  = Y[split:]
        
        return train_x, train_y, test_x, test_y
    '''
    Description:
        Trains based on X data
    Parameters:
        X: features
        Y: labels
        training_split: percentage of training split, [0.0 to 1.0]
        evaluate_when: at what iterations should the trained NN be tested
        sgd_lr: learning rate, effective rate will be sgd_lr/t
        p: stops training when the error on the validation set is >= then the last p times
    '''
    def train(self, X, Y, training_split=0.5, T=1000, batch_size=1, evaluate_when=1, sgd_lr=5, p=5, phi=-1.0):
        accs       = []
        old_acc    = 0
        p_eval     = 0
        acc        = 0
        # Split data based on wanted split
        tr_x, tr_y, val_x, val_y = self.split_data(X, Y, training_split)
        preds = np.full((len(val_x),1), 0)
        # SGD
        for t in range(1, T+1):
            lr = sgd_lr/t
            # take the gradient step
            batch_y,loss = self.gradient_step(tr_x, tr_y, batch_size, lr, phi)
            # Validate loss and accuracy on certain iterations
            if (t%evaluate_when == 0):
                # Get classification for val data
                for s in range(len(val_x)):
                    preds[s] = self.classify(val_x[s])
                acc = self.accuracy(val_y, preds)
                accs.append(acc)
                # Stop training as soon as the error on the validation 
                # set is higher than it was the last p times was checked.
                if(old_acc <= acc):
                    old_acc = acc
                    p_eval += 1
                    # check for previous validation accuracy
                    if(p_eval >= p):
                        self.acc = old_acc
                        return accs
                else:
                    p_eval = 0
        self.acc = old_acc
        return accs
def task_2(method, with_phi, X, Y, t_X, t_Y):
    ann = ANN(HIDDEN_LAYERS, method, sigmoid, 
            sigmoid_prime, cross_entropy, cross_entropy_prime)
    atleast = 5
    tot_acc = 0
    # train
    if(with_phi == True):
        print("   Lamda/Phi combo used: {}/{}".format(7,0.7))
    else:    
        print("   Lamda used: {}".format(7))
    for _ in range(atleast):
        # train
        if(with_phi == True):
            ann.train(X, Y, SPLIT, ITERS, BATCH, EVAL, sgd_lr=7, p=5, phi=0.7)
        else:
            ann.train(X, Y, SPLIT, ITERS, BATCH, EVAL, sgd_lr=7, p=5)
            
        # test ANN
        preds = np.full((len(t_X),1), 0)
        for s in range(len(t_X)):
            preds[s] = ann.classify(t_X[s])
        acc = ann.accuracy(t_Y, preds)
        tot_acc += ann.acc
    print("   {:.2f}%".format(tot_acc/atleast*100))
'''
    MAIN
'''
if __name__ == "__main__":
    # Extract data and change the -1 to a 0
    X,Y = extract_data('../data_sets/DWH_Training.csv', shuffle=True)
    # Get classification for test data
    data = pd.read_csv('../data_sets/DWH_test.csv', delimiter=',', 
                names=['index', 'height', 'weight', 'gender', 'unknown'])
    del data['unknown']
    data.to_csv(path_or_buf='../data_sets/DWH_Test.csv', index=False, header=False)
    t_X,t_Y = extract_data('../data_sets/DWH_Test.csv', shuffle=False)
      
    # initialize ANN
    ann = ANN(HIDDEN_LAYERS, method1, sigmoid, 
                sigmoid_prime, cross_entropy, cross_entropy_prime)
    # X, Y, training_split=0.5, T=1000, batch_size=1, evaluate_when=1, sgd_lr=5, p=5, phi=-1.0
    ##########################################
    ################# TASK 1 #################
    ##########################################
    print("Task 1.a")
    # train ANN, no momentum
    ann.train(X, Y, SPLIT, ITERS, BATCH, EVAL, sgd_lr=5, p=5)
    print("   No momentum:")
    print("      lamda: {}".format(5))
    print("      training validation acc.: {:.2f}%".format(ann.acc*100))
    # test ANN
    preds = np.full((len(t_X),1), 0)
    for s in range(len(t_X)):
        preds[s] = ann.classify(t_X[s])
    acc = ann.accuracy(t_Y, preds)
    print("      testing validation acc.:  {:.2f}%".format(ann.acc*100))
    # train ANN, with momentum
    ann.train(X, Y, SPLIT, ITERS, BATCH, EVAL, sgd_lr=5, p=5, phi=0.5)
    print("   With momentum:")
    print("      lamda/phi: {}".format(5,0.5))
    print("      training validation acc.: {:.2f}%".format(ann.acc*100))
    # test ANN
    preds = np.full((len(t_X),1), 0)
    for s in range(len(t_X)):
        preds[s] = ann.classify(t_X[s])
    acc = ann.accuracy(t_Y, preds)
    print("      testing validation acc.:  {:.2f}%".format(ann.acc*100))
    
    # evaluate different phis and lamdas
    print("Task 1.b")
    lamdas    = []
    best_phis = []
    best_accs = []
    atleast = 3
    for l in range(1,11,1):
        phis    = []
        accs    = []
        for p in np.arange(0.1,1.0,0.1):
            tot_acc = 0
            for _ in range(atleast):
                # train
                ann.train(X, Y, SPLIT, ITERS, BATCH, EVAL, sgd_lr=l, p=5, phi=p)
                # test ANN
                preds = np.full((len(t_X),1), 0)
                for s in range(len(t_X)):
                    preds[s] = ann.classify(t_X[s])
                acc = ann.accuracy(t_Y, preds)
                tot_acc += ann.acc
            phis.append(p)
            accs.append(tot_acc/atleast)
        # now lets evaluate the best accuracy for the phi
        best_acc = 1.0
        best_idx = 0
        for i in range(len(accs)):
            if(accs[i] < best_acc):
                best_acc = accs[i]
                best_idx = i
        # get the best phi
        best_phis.append(phis[best_idx])
        best_accs.append(best_acc)
        lamdas.append(l)
    # Check which lamda had the better accuracy
    bestest_acc = 1.0
    bestest_idx = 0
    for i in range(len(lamdas)):
        if(best_accs[i] < bestest_acc):
            bestest_acc = best_accs[i]
            bestest_idx = i
    print("   Best Lamda/Phi combo is: {}/{} with {:.2f}% accuracy".\
        format(lamdas[bestest_idx],best_phis[bestest_idx],bestest_acc*100))
    most_best_l = lamdas[bestest_idx]
    most_best_p = best_phis[bestest_idx]
    
    ##########################################
    ################# TASK 2 #################
    ##########################################
    # A
    print("\nTask 2.a - Unifrom Random [0,1] (method2)")
    task_2(method2, True, X, Y, t_X, t_Y)
    print("Task 2.a - All Zeros (method3)")
    task_2(method3, True, X, Y, t_X, t_Y)
    print("Task 2.a - Normal Distr. [-10,10] (method4)")
    task_2(method4, True, X, Y, t_X, t_Y)

    # B
    print("\nTask 2.b - Unifrom Random [0,1] (method2)")
    task_2(method2, False, X, Y, t_X, t_Y)
    print("Task 2.b - All Zeros (method3)")
    task_2(method3, False, X, Y, t_X, t_Y)
    print("Task 2.b - Normal Distr. [-10,10] (method4)")
    task_2(method4, False, X, Y, t_X, t_Y)
    
'''
Output

Task 1.a
   No momentum:
      lamda: 5
      training validation acc.: 91.67%
      testing validation acc.:  86.67%
   With momentum:
      lamda/phi: 5
      training validation acc.: 91.67%
      testing validation acc.:  84.44%
Task 1.b
   Best Lamda/Phi combo is: 1/0.4 with 84.44% accuracy

Task 2.a - Unifrom Random [0,1] (method2)
   Lamda/Phi combo used: 7/0.7
   71.11%
Task 2.a - All Zeros (method3)
   Lamda/Phi combo used: 7/0.7
   44.44%
Task 2.a - Normal Distr. [-10,10] (method4)
   Lamda/Phi combo used: 7/0.7
   67.11%

Task 2.b - Unifrom Random [0,1] (method2)
   Lamda used: 7
   44.44%
Task 2.b - All Zeros (method3)
   Lamda used: 7
   44.44%
Task 2.b - Normal Distr. [-10,10] (method4)
   Lamda used: 7
   84.44%

Done.
'''
